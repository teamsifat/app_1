package first_app.sifatcste08.com.example.firstapps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textView1,textView2;
        final Button button1;
        final EditText editText1;

        textView1=findViewById(R.id.tv1);
        textView2=findViewById(R.id.tv2);
        button1=findViewById(R.id.b1);
        editText1=findViewById(R.id.et1);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String myText=editText1.getText().toString();
                button1.setText("I was clicked");
                textView1.setText(myText);
            }
        });

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                textView1.setText("Our University");
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                textView2.setText("Our department");
            }
        });
    }






}
